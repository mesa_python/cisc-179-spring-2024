

# Math Caves Verson 2.0.0
# Programmed by Rowan Cutler for Hobson Lane's CISC 179


''' This is Math Caves v2! In this version, fixed the variable errors that broke 
 the program; had to make the variable global; in different functions, it did not 
 read the variable in the previous groups. Any string that is not 
 part of the problem generation will now be data driven, meaning it will
 now be in a text file allowing ease in controlling what is being said to the players.'
 Hacking this file is allowed, but you better not break my game 😡  (JK, have fun)'''


#                    !!!IMPORTANT!!!
#       ⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️
#   In order for Math caves to complile game data, we must access the 'Cave_Data' file
# After downloading this file, right click, copy this file path, paste it below within the " "
File_Path = r"Cave_Data.txt"
#      ⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️

# Thanks for playing!

import time
import random


f = open(File_Path, "r")
ContentList = []
for I in f.readlines():
    ContentList.append(I)
print(ContentList[1])

time.sleep(0.5)
RightAnswer = 0
Problem_Saves = {"Add": [], "Sub": [], "Mult": [], "Div": []}


time.sleep(1)
Start_Countdown = 5

for Number in range(Start_Countdown):
    print("Game starting in " + str(Start_Countdown))
    Start_Countdown = Start_Countdown - 1
    time.sleep(1)

print(ContentList[2])

time.sleep(0.1)
cave1done = False
cave2done = False
cave3done = False
cave4done = False


problemCorrect = 0


def addition():

    beginningTime = int(time.time())
    NumberOne = random.randint(1, 10)
    NumberTwo = random.randint(1, 10)
    Trial = input("What is " + str(NumberOne) + " + " + str(NumberTwo) + "?")
    endingTime = int(time.time())

    if int(endingTime - beginningTime) > 5:

        print(ContentList[3])
    else:
        if int(NumberOne + NumberTwo) == int(Trial):

            print(ContentList[4])
            global problemCorrect

            problemCorrect = problemCorrect + 1
        else:

            print(ContentList[5])
            Problem_Saves["Add"].append(str(NumberOne) + " " + str(NumberTwo))


def subtraction():
    beginningTime = int(time.time())
    NumberOne = random.randint(1, 10)
    NumberTwo = random.randint(1, 10)
    NumberThree = NumberOne + NumberTwo
    Trial = input("What is " + str(NumberThree) + " - " + str(NumberTwo) + "?")
    endingTime = int(time.time())

    if int(endingTime - beginningTime) > 5:

        print(ContentList[3])
    else:
        if int(NumberOne) == int(Trial):

            print(ContentList[4])
            global problemCorrect

            problemCorrect = problemCorrect + 1
        else:

            print(ContentList[5])
            Problem_Saves["Sub"].append(str(NumberThree) + " " + str(NumberTwo))


def multiplication():
    beginningTime = int(time.time())
    NumberOne = random.randint(1, 10)
    NumberTwo = random.randint(1, 10)
    NumberThree = NumberOne * NumberTwo
    Trial = input("What is " + str(NumberOne) + " x " + str(NumberTwo) + "?")
    endingTime = int(time.time())

    if int(endingTime - beginningTime) > 5:

        print(ContentList[3])
    else:
        if int(NumberThree) == int(Trial):

            print(ContentList[4])
            global problemCorrect

            problemCorrect = problemCorrect + 1
        else:

            print(ContentList[5])
            Problem_Saves["Mult"].append(str(NumberOne) + " " + str(NumberTwo))


def division():
    beginningTime = int(time.time())
    NumberOne = random.randint(1, 10)
    NumberTwo = random.randint(1, 10)
    NumberThree = NumberOne * NumberTwo
    Trial = input("What is " + str(NumberThree) + " Divided by " + str(NumberTwo) + "?")
    endingTime = int(time.time())

    if int(endingTime - beginningTime) > 5:

        print(ContentList[3])
    else:
        if int(NumberOne) == int(Trial):

            print(ContentList[4])
            global problemCorrect

            problemCorrect = problemCorrect + 1
        else:

            print(ContentList[5])
            Problem_Saves["Div"].append(str(NumberThree) + " " + str(NumberTwo))


while cave1done == False:
    if problemCorrect > 9:
        cave1done = True

    if cave1done == False:
        randomnumber = random.randint(1, 4)
        if randomnumber == 1:
            addition()
        elif randomnumber == 2:
            subtraction()
        elif randomnumber == 3:
            division()
        elif randomnumber == 4:
            multiplication()

    elif cave1done == True:
        if str(Problem_Saves.values()) == "dict_values([[], [], [], []])":

            print(ContentList[6])

            print(ContentList[7])
        else:
            print(ContentList[8])

            Start_Countdown = 10
            time.sleep(5)
            print(" ")
            print(" ")
            print(" ")

    for Number in range(Start_Countdown):
        print("The masters chamber ultimate trial testing begins in " + str(Start_Countdown))
        Start_Countdown = Start_Countdown - 1
        time.sleep(1)
        print(" ")
        print(" ")


def AddTrial():
    for I in Problem_Saves["Add"]:

        print(ContentList[11])
        Numbers = I.split()
        global Trial
        Trial = input("What is " + str(Numbers[0]) + " + " + str(Numbers[1]) + "?")
        global RightAnswer
        RightAnswer = int(Numbers[0]) + int(Numbers[1])

    if RightAnswer == int(Trial):

        print(ContentList[9])
    else:

        print(ContentList[10])
        AddTrial()


def LessTrial():
    for I in Problem_Saves["Sub"]:

        print(ContentList[12])
        Numbers = I.split()
        global Trial
        Trial = input("What is " + str(Numbers[0]) + " - " + str(Numbers[1]) + "?")
        global RightAnswer
        RightAnswer = int(Numbers[0]) - int(Numbers[1])

    if RightAnswer == int(Trial):

        print(ContentList[9])
    else:
        print(ContentList[10])

        LessTrial()


def MultTrial():
    for I in Problem_Saves["Mult"]:

        print(ContentList[13])
        Numbers = I.split()
        global Trial
        Trial = input("What is " + str(Numbers[0]) + " x " + str(Numbers[1]) + "?")
        global RightAnswer
        RightAnswer = int(Numbers[0]) * int(Numbers[1])

    if RightAnswer == int(Trial):

        print(ContentList[9])
    else:
        print(ContentList[10])

        MultTrial()


def DivTrial():
    for I in Problem_Saves["Div"]:

        print(ContentList[14])
        Numbers = I.split()
        global Trial
        Trial = input("What is " + str(Numbers[0]) + " Divided by " + str(Numbers[1]) + "?")
        global RightAnswer
        RightAnswer = int(Numbers[0]) / int(Numbers[1])

    if RightAnswer == int(Trial):
        print(ContentList[9])

    else:
        print(ContentList[10])

        DivTrial()


AddTrial()
LessTrial()
MultTrial()
DivTrial()

print(ContentList[7])


MultNumberOne = random.randint(1, 12)
MultNumberTwo = random.randint(1, 12)
