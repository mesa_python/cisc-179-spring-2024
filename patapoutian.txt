&main
Welcome to Inside the Park, a game in which you get to be the GM of a baseball
team for a year. The goal is to win the World Series this year.
Would you like a tutorial. If yes enter (Y)es and if not enter (N)o.


#main

&tutorial
In this game you will be offered choices that will have an impact on the
season of your team. These choices will have a 'right' and 'wrong' answer;
however there is still some randomness in the results.
Your first choice is who to hire as manager of your team. The first
option is promoting Wiley Griggs, who has served as the teams bench coach,
(second in command), or to hire Miguel Mejia, a ex-player looking for their
first managerial job after retiring. Make your choice by typing their name
or initials.
#tutorial

&005
Going with someone already in the organization helps keep the clubhouse
happy with a consistent philosophy. Gain a 5% boost to all future choices.


#005

&000
Miguel certainly isn't a bad manager, but the quick change in style from last
year means you don't gain any boost to future choices.


#000

&spring_training
Welcome to spring training! Its a new year of baseball. The first item
on today's to do list is deciding which of two veterans to give a chance to
join your team. First is Shane Reynolds, a speedy outfielder who has a
pentient for getting hit by pitches. Last year he put up a .250 batting
average, a .357 on base percentage and a .340 slugging for an adjusted
OPS of 84 [OPS+ is a good summary of a players offensive output, where 100
is league average]. The other option is Curley Williams, who is not as good
defensively but hits significantly more home runs. His slashline was
.269/.299/.413 for a comparable OPS+ of 82. Make your choice by typing their
name or initials.
#spring_training

&11a
Shane has a breakout year helping your team get out to a 24-16 start.

#11a

&10a
Shane is unable to break into your team, and your team gets off to a slow
start of 17-23.

#10a

&11b
Curley has a breakout year helping your team get out to a 24-16 start.

#11b

&10b
Curley is unable to break into your team, and your team gets off to a slow
start of 17-23.

#10b

&prospect
One of your top prospects, Christian Walker is hitting really well in your
minor league team, with an OPS of 1.218 which is extremely high. Your
coaches are asking you if now is the right time to promote him to the Majors.
Enter Yes to promote Walker to your team, or No to let him develop for the
future.
#prospect

&22a
Walker has an immediate impact and is being hailed as a generational
player. Your team surges to 48-32.

#22a

&21a
Walker has an immediate impact and is being hailed as a generational
player. Your team surges to 41-39.

#21a

&21b
Walker isn't able to significantly contribute to your team, which now has a
record of 41-39.

#21b

&20b
Walker isn't able to significantly contribute to your team, which now has a
record of 35-45.

#20b

&21c
Without Walker your team reaches a 41-39 record after 80 games.

#21c

&20c
Without Walker your team's record is only 35-45

#20c

&trade
It is nearing the deadline to make trades and two teams are offering you
star players to help you make a push for the playoffs. Option one is the 40
year old DH, meaning he doesn't play defense, Terry Cox. Cox has won the
Silver Slugger, the award for best hitter at a position, two years
consecutively as well as coming top 10 in MVP voting. His OPS+ for this year
is 159. A different team is also offering you a DH. They propose you trade
for 29 year old Ferris Fain. Fain is two years removed from an All-Star
selection and a Silver Slugger of his own, and has put up an adjusted OPS+
of 165 this year. Make your choice by typing their name or initials.
#trade

&31a
Cox continues his stellar season and will get votes for MVP after the
postseason. Your team's record is now 61-59.

#31a

&32a
Cox continues his stellar season and will get votes for MVP after the
postseason. Your team's record is now 67-63.

#32a

&33a
Cox continues his stellar season and will get votes for MVP after the
postseason. Your team continues its good form reaching a record of 74-46.

#33a

&30b
Cox is unable to recapture his first half success. Your team is now 50-70.

#30b

&31b
Cox is unable to recapture his first half success. Your team is now 61-59.

#31b

&32b
Cox is unable to recapture his first half success. Your team is now 67-63.

#32b

&31c
Fain continues his stellar season and will get votes for MVP after the
postseason. Your team's record is now 61-59.

#31c

&32c
Fain continues his stellar season and will get votes for MVP after the
postseason. Your team's record is now 67-63.

#32c

&33c
Fain continues his stellar season and will get votes for MVP after the
postseason. Your team's record is now 61-59.

#33c

&30d
Fain is unable to recapture his first half success. Your team is now 50-70.

#30d

&31d
Fain is unable to recapture his first half success. Your team is now 61-59.

#31d

&32d
Fain is unable to recapture his first half success. Your team is now 67-63.

#32d

&secret0_1
You trade away top prospect Christian Walker to get both players. Your
team is now 61-59.

#secret0_1

&secret0_2
You trade away top prospect Christian Walker to get both players. Your
team is now 67-63.

#secret0_2

&secret1
You do not have the prospect capital to trade for both players.

#secret1

&injury
Disaster strikes! Your star 3rd baseman has sustained a season ending injury.
There are two players on your minor league team who can play his position.
The first is Marv Grissom. In the previous month Marv changed his hitting
approach for minor gains. He is swinging at strikes 65% of the time up from
62%, while reducing his swings at pitches outside the zone to 25% from 30%.
Meanwhile Darrell Evans is on a hot streak of his own, batting .389 in the
last month. Make your choice by typing their name or initials.
#injury

&41a
Marv Grissom is able to translate better mindset to better results. Your
team ends the season with a record of 79-83. You have unfortunately missed
the playoffs.

#41a

&42a
Marv Grissom is able to translate better mindset to better results. Your
team ends the season with a record of 86-76. You have made it to the playoffs!

#42a

&43a
Marv Grissom is able to translate better mindset to better results. Your
team ends the season with a record of 91-71. You have made it to the playoffs!

#43a

&44a
Marv Grissom is able to translate better mindset to better results. Your
team ends the season with a record of 101-61. This secures you a first
round bye!

#44a

&40b
Marv Grissom is unable to replace lost production. Your team ends the
season with a record of 71-91. You have unfortunately missed the playoffs.

#40b

&41b
Marv Grissom is unable to replace lost production. Your team ends the
season with a record of 79-83. You have unfortunately missed the playoffs.

#41b

&42b
Marv Grissom is unable to replace lost production. Your team ends the
season with a record of 86-76. You have made it to the playoffs!

#42b

&43b
Marv Grissom is unable to replace lost production. Your team ends the
season with a record of 91-71. You have made it to the playoffs!

#43b

&41c
Darrell Evans's hot streak continues. Your team ends the season with a
record of 79-83. You have unfortunately missed the playoffs.

#41c

&42c
Darrell Evans's hot streak continues. Your team ends the season with a
record of 86-76. You have made it to the playoffs!

#42c

&43c
Darrell Evans's hot streak continues. Your team ends the season with a
record of 91-71. You have made it to the playoffs!

#43c

&44c
Darrell Evans's hot streak continues. Your team ends the season with a
record of 101-61. This secures you a first round bye!

#44c

&40d
Darrell Evans's hot streak ends abruptly. Your team ends the season with
a record of 71-91. You have unfortunately missed the playoffs.

#40d

&41d
Darrell Evans's hot streak ends abruptly. Your team ends the season with
a record of 79-83. You have unfortunately missed the playoffs.

#41d

&42d
Darrell Evans's hot streak ends abruptly. Your team ends the season with
a record of 86-76. You have made it to the playoffs!

#42d

&43d
Darrell Evans's hot streak ends abruptly. Your team ends the season with
a record of 91-71. You have made it to the playoffs!

#43d

&wild_card_txt
Welcome to the playoffs! Your team dominates game one behind 8 innings
without a run from your Ace starting pitcher, and a 1-2-3 9th from your
closer. However game two goes the other way due to your relief pitchers's
poor showing out of the bullpen. Its the 6th inning out of 9 in game three,
and your team is holding on to a one run lead. However your starting pitcher
is running out of steam. Do you trust your relief pitchers to close the game
out or do you want to bring your ace bback out on one day of rest to try to
make it to the 9th inning so only your closer has to pitch? Make your
selection by typing Bullpen or Starter.
#wild_card_txt

&wc_w1
Your bullpen pulls through. You have made it to the next round!

#wc_w1

&wc_l1
Your bubllpen wasn't able to see the game out. You have been knocked out
of the playoffs.

#wc_l1

&wc_w2
Your ace is having the playoff performance of a lifetime. The lead is held
and you have made it to the next round!

#wc_w2

&wc_l2
Especially with their heavy workload just two days ago your ace is unable
to keep the other team at bay. You have been knocked out.

#wc_l2

&division_series
Just two series from lifting the World Series trophy! Unfortunately you are
already down two games in a best of five. You need to win the next three to
advance. You could stay with the statistically 'optimal' order you have been
using all season, or you could mix the order up to try to jumpstart your
offense. Make your selection by typing Stay or Mix.
#division_series

&ds_w1
Your team comes in clutch and is able to win the next three games to make
it to the semi-finals!


#ds_w1

&ds_l1
Your team isn't able to turn it around and you have been knocked out of
the playoffs.


#ds_l1

&ds_w2
The new order jumpstarts your offense, which secures you the series win.


#ds_w2

&ds_l2
Nothing seems to be able to restart your offense and you have been knocked out
of the playoffs.
#ds_l2

&championship_series
Game seven to decide who goes to the World Series. Its win or you are out.
If not many people remember runners-up they sure won't remember those who
tied for 3rd. It's now the bottom of the 8th. A big home run in your half
gives your team a nervous one run lead. The three batters you have to face
this inning are
1) your opponent's defensive wizard of a shortstop, who is there much more
for his glove than his bat.
2) the opposing second baseman, well know for his speed and ability to
get bat to ball. He was chasing the first .400 batting average season in
50 years until the final month of the regular season.
3) one of the favorites to win this year's MVP award. This center fielder
hits absolute rockets into the stands, and is always a home run threat.
Their lineup doesn't get harder than this. Even though closers typically
only pitch the last inning to secure the win, he is your best available
pitcher. Do you send him out in the 8th or the 9th? Type eight(8)/nine(9) to
make your choice.
#championship_series

&cs_w1
This was one of the all time greatest playoff series. Your city will remember
the energy when your closer fooled the center fielder with a beautiful
curveball for a long time. Energy only matched by the jubilation of all on
the field after confirming that victory in the 9th. Good luck in the
World Series!

#cs_w1

&cs_l1
This was one of the all time greatest playoff series. Unfortunately that
joy will go to opposing fans as their comeback game seven victory will
become a core franchise moment.

#cs_l1

&cs_w2
Though your bullpen has been hit and miss at best this playoffs they come
through when it matters most(ish). The celebrations across the city when
your closer fooled their last hitter with a beautiful curveball to close
out the game were simply amazing. Good luck in the World Series!

#cs_w2

&cs_l2
Your closer doesn't even get the chance to pitch in the 9th, as a lead
stealing two run home run crushes your hopes for this year. Still this
was one of the greatest playoff series of all time, and your team put
up a great fight.

#cs_l2

&world_series
Its game 5 of the World Series. Your team has the chance to win it here
and now. Fortunately your ace is on the mound and has silenced the opposing
offense. Entering the 7th inning they are set to face the best three
opposing hitters. You know that batters get significantly better, adding 70
points of OPS, when seeing a pitcher for the third time in a game. On the
other hand, your ace has been pitching very well, why break that up? Type
Bullpen to use relievers to see this game out Starter to keep your ace in.
#world_series

&ws_w1
Your bullpen continues the dominance shown off today, and your offense
comes alive to secure the trophy! Congratulations on becoming World Series
Champions! It's time to pop some champaign and celebrate!

#ws_w1

&ws_l1
A big 8th inning rally gives the other team the lead. Unfortunately this
game was the closest you will get to lifting the trophy.

#ws_l1

&ws_w2
our ace's performance in this year's playoffs will go down in the history
books as one of the greatest of all time, and will probably secure their spot
in the Hall of Fame. Congratulations on winning the World Series and getting
to lift that trophy! It's time to pop some champaign and celebrate!

#ws_w2

&ws_l2
Though heroic, your ace's attemps are, in the end, futile. With the massive
workload they have put on their arm, it was bound to give up at some point.
You have come just short of lifting the trophy.

#ws_l2

&endscreen_txt
Would you like to start again? If yes type retry, and if not type exit.
Thanks for playing!
#endscreen_txt