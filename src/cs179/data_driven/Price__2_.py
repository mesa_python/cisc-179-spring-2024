def read_markdown(path = 'text_adventure.md'):
    with open(path) as f:
        lines = f.readlines()

    graph = {}
    first_room = None
    for line in lines:
        if line[0] == '#':
            key = line.strip('#').strip().lower()
            graph[key] = {}
            if first_room is None:
                first_room = key
        elif line[1] == ':':
            graph[key][line[0]] = line[2:].strip().lower()
        else:
            graph[key]['description'] = line.strip()
    return graph, first_room

def play_game(graph, first_room):
    room = first_room
    visited_rooms = []

    while room:
        visited_rooms.append(room)
        print(graph[room]['description'])
        if len(graph[room]) > 1:
            command = ' '
            while command not in graph[room]:
                command = input('>>> ')
            room = graph[room][command]
        else:
            room = ' '
    return visited_rooms

if __name__== '__main__':
    graph, first_room = read_markdown('text_adventure.md')
    rooms = play_game(graph, first_room=first_room)