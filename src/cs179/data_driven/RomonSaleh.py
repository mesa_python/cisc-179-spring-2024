import json

def load_game_data(filename):
    with open(filename, 'r') as file:
        return json.load(file)

game_data = load_game_data('game_data.json')
def main():
    game_data = load_game_data('game_data.json')
    current_room = "entrance"
    score = 0
    visited_rooms = set()

    while True:
        room = game_data["rooms"][current_room]
        print(room["description"])

        if current_room not in visited_rooms:
            score += 1
            visited_rooms.add(current_room)

        command = input("What do you do? ").strip().lower()

        if command in room["commands"]:
            outcome = room["commands"][command]
            if outcome in game_data["rooms"]:
                current_room = outcome
            else:
                print(outcome)
        else:
            print("You can't do that.")

        if current_room == "game_over":
            break

    print(f"Game Over. Your score is {score}")

if __name__ == "__main__":
    main()
