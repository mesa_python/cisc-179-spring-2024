## Arrival
You arrive at the castle gates, an aura of evil blankets the castle. Would you like to go into the [A] front door or take a shortcut through the [B] moat area?
A: front
B: moat
X: exit
## moat
Chains wrap around your limbs, the area fills with water, and you drown. Game over.
 : exit
## front
You enter the main hall where Lilith awaits you. Do you throw [C] holy water at Lilith to try to kill her or use your .45 handgun to [D] shoot her heart?
C: throw holy water
D: shoot
X: exit
## throw holy water
You throw holy water in an attempt to kill her. She blocks your attack with her wings and then decapitates you. Game over.
 : exit
## shoot
You aim your .45 handgun at her heart and shoot. You kill her instantly and head to Dracula's throne room. After you think you've killed Lilith, she reincarnates in front of you. Do you [E] use holy water again or [F] bring out your whip?
E: use holy water
F: whip
X: exit
## whip
She dodges and decapitates you. Game over.
 : exit
## use holy water
Holy water works this time and you proceed further into the castle. After defeating Lilith, you enter Dracula's throne room. There is a puzzle you must solve in order to reach the secret area. Do you solve the puzzle by [G] trying to open the door using the levers or [H] attempting to pry the door open without the levers?
G: levers
H: door
X: exit
## levers
The levers are a trap. Acid pours down on you and you die. Game over.
 : exit
## door
You enter a room full of mirrors. Do you [I] break the mirrors with your whip or [J] use holy water to douse them?
I: break
J: douse
X: exit
## break
The mirrors start to break under your whip, but a trap door opens and you fall to your death. Game over.
 : exit
## douse
After dousing the mirrors with holy water, Dracula screams then appears for a final battle. When the battle begins, do you [K] charge head on with your whip out, [L] take cover behind a mirror, [M] throw holy water at Dracula, [N] run away, [O] use crucifix to scare Dracula, or [P] try to convince to Dracula to stand down?
K: charge
L: take cover
M: throw holy water at Dracula
N: run
O: crucifix
P: stand down
X: exit
## charge
Dracula stuns you with his cape and rips your heart out. Game over.
 : exit
## take cover
Dracula hesitates and the battle continues. The battle with Dracula continues, and after dodging multiple attacks an opening finally appears. Do you use your [Q] whip at his head, [R] continue to dodge, [S] use agility to dodge and then strike with your whip, [T] use holy water to create a barrier, [U] use your stake to strike Dracula's heart, or [V] tell Dracula to stand down before he perishes?
Q: use whip
R: dodge
S: agility
T: barrier
U: stake
V: talk
X: exit
## throw holy water at Dracula
Dracula is stunned, and the battle continues. The battle with Dracula continues, and after dodging multiple attacks an opening finally appears. Do you use your [Q] whip at his head, [R] continue to dodge, [S] use agility to dodge and then strike with your whip, [T] use holy water to create a barrier, [U] use your stake to strike Dracula's heart, or [V] tell Dracula to stand down before he perishes?
Q: use whip
R: dodge
S: agility
T: barrier
U: stake
V: talk
X: exit
## run
Dracula calls you a coward and uses fire to burn you. Game over.
 : exit
## crucifix
Dracula laughs and tells you to try again. The battle with Dracula continues, and after dodging multiple attacks an opening finally appears. Do you use your [Q] whip at his head, [R] continue to dodge, [S] use agility to dodge and then strike with your whip, [T] use holy water to create a barrier, [U] use your stake to strike Dracula's heart, or [V] tell Dracula to stand down before he perishes?
Q: use whip
R: dodge
S: agility
T: barrier
U: stake
V: talk
X: exit
## stand down
Dracula scoffs and lunges to rip your heart out. Game over.
 : exit
## use whip
The whip stuns Dracula, and the battle continues. The battle rages on and you can feel your stamina dwindling. However, Dracula is also breaking a sweat. What's your next move? Do you [W] Grandstand to trick Dracula, [Y] go on an all-out assault using your whip and dagger, or [Z] stay on defense and repel any attacks?
W: grandstand
Y: assault
Z: defense
X: exit
## dodge
Dracula corners you and then feasts on you. Game over.
 : exit
## agility
You gain the upper hand, and the battle continues. The battle rages on and you can feel your stamina dwindling. However, Dracula is also breaking a sweat. What's your next move? Do you [W] Grandstand to trick Dracula, [Y] go on an all-out assault using your whip and dagger, or [Z] stay on defense and repel any attacks?
W: grandstand
Y: assault
Z: defense
X: exit
## barrier
A Chandelier falls on you and kills you. Game over.
 : exit
## stake
You strike too soon...Dracula decapitates you. Game over.
 : exit
## talk
Dracula boldly laughs, and the battle continues. The battle rages on and you can feel your stamina dwindling. However, Dracula is also breaking a sweat. What's your next move? Do you [W] Grandstand to trick Dracula, [Y] go on an all-out assault using your whip and dagger, or [Z] stay on defense and repel any attacks?
W: grandstand
Y: assault
Z: defense
X: exit
## grandstand
Dracula is confused and ends up hurting himself in his confused state with his fire, and the battle continues. The final battle is almost over. What is your last move? Do you [1] use Razzle Dazzle to wrap your whip around Dracula's neck and stab his heart? Or do you [2] try to use holy water to ensnare Dracula?
1: razzle dazzle
2: ensnare
X: exit
## assault
You slit each other's throats and you both die. Game over.
 : exit
## defense
The battle rages on. The final battle is almost over. What is your last move? Do you [1] use Razzle Dazzle to wrap your whip around Dracula's neck and stab his heart? Or do you [2] try to use Holy water to ensnare Dracula?
1: razzle dazzle
2: ensnare
X: exit
## razzle dazzle
It succeeds and you beat Dracula. The battle is over when, suddenly, someone appears to offer you a reward. Do you select [3] sister wives and money or [4] money and property?
3: sister wives and money
4: money and property
X: exit
## ensnare
It fails, and Dracula kills you. Game over.
 : exit
## sister wives and money
You get attractive and fit sister wives and unlimited money. If there is a divorce, you keep the money and kids. The women pay you child support and alimony.
 : exit
## money and property
You get unlimited money and property in the country of your choice.
 : exit