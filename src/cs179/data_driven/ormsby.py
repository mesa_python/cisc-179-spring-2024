
import json

def load_game_data(filepath):
    with open(filepath, 'r') as file:
        return json.load(file)

def main():
    game_data = load_game_data('expanded_game_data.json')
    current_room = 'Varrock Tavern'  # Start the game in the initial room

    print("Welcome to Runescape!")
    print("You log into the game and your character loads into Gielinor.")

    game_over = False

    while not game_over:
        room = game_data['rooms'][current_room]
        print(room['description'])

        for action_key, action in room['actions'].items():
            print(action['prompt'])
            player_choice = input("> ").lower()
            
            if player_choice in action['responses']:
                print(action['responses'][player_choice])

                # Handle specific transitions based on player choices
                if current_room == 'Varrock Tavern' and action_key == 'leave' and player_choice == 'yes':
                    current_room = 'King of Varrock'
                elif current_room == 'King of Varrock' and action_key == 'speak' and player_choice == 'no':
                    current_room = 'Demon Impling'
                elif current_room == 'Demon Impling' and action_key == 'catch' and player_choice == 'yes':
                    current_room = 'Fishing'
                elif current_room == 'Fishing' and action_key == 'fish' and player_choice == 'yes':
                    current_room = 'Dwarven Mines'
                elif current_room == 'Dwarven Mines' and action_key == 'explore' and player_choice == 'yes':
                    print("You've successfully explored the mines and your adventure ends with treasures!")
                    game_over = True

            else:
                print("Invalid choice. Please try again.")
        
        if not game_over:
            print("\nWhat would you like to do next?")

if __name__ == '__main__':
    main()
