import sys
with open('binauhan.txt') as f:
    rooms = f.readlines()

def start_game():
    print(rooms[0])
    print(rooms[1])
    
    character_class = input(rooms[2])
    
    if character_class == 'a':
        print(rooms[4])
        warrior_fight()
    elif character_class == 'b':
        print(rooms[5])
        mage_fight()
    elif character_class == 'c':
        print(rooms[6])
        rogue_fight()

def warrior_fight():
    print(rooms[8])
    print(rooms[10])
    
    warrior_choice1 = input(rooms[14])
    
    if warrior_choice1 == 'a':
        print(rooms[15])
        warrior_fight_part2()
    elif warrior_choice1 == 'b':
        print(rooms[16])
        game_over()
    elif warrior_choice1 == 'c':
        run_away()
        
def warrior_fight_part2():
    print(rooms[18])
    
    warrior_choice2 = input(rooms[20])
    
    if warrior_choice2 == 'a':
        print(rooms[21])
        game_over()
    elif warrior_choice2 == 'b':
        print(rooms[22])
        good_ending()
        
def mage_fight():
    print(rooms[8])
    print(rooms[11])
    
    mage_choice1 = input(rooms[24])
    
    if mage_choice1 == 'a':
        print(rooms[25])
        game_over()
    elif mage_choice1 == 'b':
        print(rooms[26])
        mage_fight_part2()
    elif mage_choice1 == 'c':
        run_away()
    
def mage_fight_part2():
    print(rooms[28])
    
    mage_choice2 = input(rooms[30])
    
    if mage_choice2 == 'a':
        print(rooms[31])
        game_over()
    elif mage_choice2 == 'b':
        print(rooms[32])
        mage_fight_part3()
        
def mage_fight_part3():
    print(rooms[34])
    
    mage_choice3 = input(rooms[36])
    
    if mage_choice3 == 'a':
        print(rooms[37])
        good_ending()
    elif mage_choice3 == 'b':
        print(rooms[38])
        game_over()
        
def rogue_fight():
    print(rooms[8])
    print(rooms[12])
    
    rogue_choice1 = input(rooms[40])
    
    if rogue_choice1 == 'a':
        print(rooms[41])
        rogue_fight_part2()
    elif rogue_choice1 == 'b':
        print(rooms[42])
        game_over()
    elif rogue_choice1 == 'c':
        run_away()
        
def rogue_fight_part2():
    rogue_choice2 = input(rooms[44])
    
    if rogue_choice2 == 'a':
        print(rooms[45])
        rogue_fight_part3()
    elif rogue_choice2 == 'b':
        print(rooms[46])
        game_over()
        
def rogue_fight_part3():
    rogue_choice3 = input(rooms[48])
    
    if rogue_choice3 == 'a':
        print(rooms[49])
        good_ending()
    elif rogue_choice3 == 'b':
        run_away()

def game_over():
    try_again = input(rooms[51])
    
    if try_again == 'yes':
        start_game()
    elif try_again == 'no':
        sys.exit()
    
def run_away():
    print(rooms[53])
    game_over()
    
def good_ending():
    print(rooms[54])
    
    new_game = input(rooms[55])
    
    if new_game == 'yes':
        start_game()
    elif new_game == 'no':
        sys.exit()
    
start_game()