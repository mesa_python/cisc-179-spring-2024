import time
import os
all_descriptions = {}
file_path = os.path.join(os.getcwd(), "karimitari.txt")
with open(file_path, "r") as file:
    for i, line in enumerate(file):
        name, description = line.strip().split(":")
        all_descriptions[name] = description


def pause(num):
    time.sleep(num)


north = 1
south = 2
east = 3
west = 4

yes = 5
no = 6

roomsChecked = []
allRoomsChecked = False

haveDiedBefore = False
global haveKey
haveKey = False


def checkForKey():
    return haveKey


def assignDoorSelection(doorSelected):
    if (doorSelected.lower().startswith('n')):
        door = north
    elif (doorSelected[0].lower() == 'e'):
        door = east
    elif (doorSelected[0].lower() == 'w'):
        door = west
    elif (doorSelected[0].lower() == 's'):
        door = south
    else:
        door = 'You must select one of the available options.'
    return door


def assignYesOrNo(answer):
    if (answer.lower().startswith('y')):
        answer = yes
    elif (answer.lower().startswith('n')):
        answer = no
    else:
        answer = 7
    return answer


def checkAnswer(answer):
    while (answer == 7):
        print(all_descriptions['notYesOrNo'])
        answer = chooseAnswer()
    return answer


def chooseAnswer():
    response = input(all_descriptions['yesOrNo'])
    answer = assignYesOrNo(response)
    return answer


def checkDoor(door):
    while (door != 1 and door != 2 and door != 3 and door != 4):
        print(all_descriptions['door'])
        door = chooseDoor()
    return door


def chooseDoor():
    print(all_descriptions['doorMessage'])
    doorInput = input('Type n(orth), s(outh), e(ast), or w(est). (Any word starting with those four letters will be interpreted as directional.)')
    door = assignDoorSelection(doorInput)
    return door


def doorSelectionOutcome(doorSelected):
    if (doorSelected in roomsChecked):
        if (doorSelected == 1 and 3 in roomsChecked):
            print('The door is still locked, but you just found a key...')
            pause(1)
            print(all_descriptions['pullOutKey'])
            pause(1)
            print(all_descriptions['keyDoesNotWork'])
        return all_descriptions['roomChecked']
    if (doorSelected == 1):
        if (3 in roomsChecked):
            print(all_descriptions['doorLocked'])
            pause(1)
            print(all_descriptions['pullOutKey'])
            pause(1)
            print(all_descriptions['keyDoesNotWork'])
        else:
            print(all_descriptions['doorLocked'])
    elif (doorSelected == 2):
        print(all_descriptions['roomTwo'])
        pause(1)
        print(all_descriptions['roomTwoWithoutKey'])
    elif (doorSelected == 3):
        print(all_descriptions['roomThree'])
        pause(1)
        print(all_descriptions['roomThreeContinued'])
        pause(1)
        print(all_descriptions['roomThreeFinish'])
    elif (doorSelected == 4):
        print(all_descriptions['roomFour'])
        print(all_descriptions['trapDoorMessage'])
        response = chooseAnswer()
        answer = checkAnswer(response)
        while (answer == 6):
            print('Are you sure? Maybe you should go check it out, just to be sure.  I might be the way to escape. Do you want to check it out?')
            response = chooseAnswer()
            answer = checkAnswer(response)
        if (answer == 5):
            print(all_descriptions['trapDoorMessageYes'])
            pause(2)
            print(all_descriptions['basementMessage'])
            pause(2)
            print(all_descriptions['basementDeath'])
            haveDiedBefore = True
            allRoomsChecked = False
            return all_descriptions['haveDiedBeforeMessage']
        print(all_descriptions['trapDoorMessageNo'])
    roomsChecked.append(doorSelected)
    print(len(roomsChecked))
    if (len(roomsChecked) == 4):
        allRoomsChecked = True
        haveKey = True
        return all_descriptions['checkAgain']
    return 'Now what?'


def doorSelectionAfterDied(doorSelected):
    if (doorSelected in roomsChecked):
        if (doorSelected == 1 and 3 in roomsChecked):
            print('The door is still locked, but you just found a key...')
            print(all_descriptions['pullOutKey'])
            pause(1)
            print(all_descriptions['keyDoesNotWork'])
        return all_descriptions['roomChecked']
    if (doorSelected == 1):
        if (3 in roomsChecked):
            print(all_descriptions['doorLocked'])
            print(all_descriptions['pullOutKey'])
            print(all_descriptions['keyDoesNotWork'])
        else:
            print(all_descriptions['doorLocked'])
    elif (doorSelected == 2):
        print(all_descriptions['roomTwo'])
        pause(1)
        print(all_descriptions['roomTwoWithoutKey'])
    elif (doorSelected == 3):
        print(all_descriptions['roomThree'])
        pause(1)
        print(all_descriptions['roomThreeContinued'])
        pause(1)
        print(all_descriptions['roomThreeFinish'])
        # putKeyInPocket()
    elif (doorSelected == 4):
        print(all_descriptions['roomFour'])
        pause(1)
        print(all_descriptions['trapDoorMessage'])
        pause(1)
        print(all_descriptions['haveDiedBeforeMessage'])
        pause(1)
        print(all_descriptions['trapDoorMessageNo'])
        pause(1)
        print(all_descriptions['goBackToFirstRoom'])
    roomsChecked.append(doorSelected)
    if (len(roomsChecked) == 4):
        allRoomsChecked = True
        haveKey = True
        return all_descriptions['checkAgain']
    return 'Now what?'


def doorSelectionSecondOutcome(doorSelected):
    if (doorSelected in roomsChecked):
        return all_descriptions['roomChecked']
    if (doorSelected == 1):
        print(all_descriptions['doorLocked'])
    elif (doorSelected == 2):
        print(all_descriptions['roomTwo'])
        pause(1)
        print(all_descriptions['roomTwoWithoutKey'])
        pause(2)
        print(all_descriptions['roomTwoWithKeyCont'])
        pause(2)
        print(all_descriptions['pullOutKey'])
        pause(1)
        print(all_descriptions['keyWorks'])
        pause(1)
        print(all_descriptions['secondToLastRoom'])
        print('escaping now')
        print(all_descriptions['escape'])
        pause(5)
        return all_descriptions['escape']
    elif (doorSelected == 3):
        print(all_descriptions['roomThree'])
        pause(1)
        print(all_descriptions['roomThreeAlreadyHaveKey'])
    elif (doorSelected == 4):
        print(all_descriptions['roomFour'])
        pause(1)
        print(all_descriptions['roomFourAfterDied'])
        pause(1)
        print(all_descriptions['goBackToFirstRoom'])
    roomsChecked.append(doorSelected)
    if (len(roomsChecked) == 4):
        allRoomsChecked = True
        haveKey = True
        return all_descriptions['checkAgain']
    return 'Now what?'


print(all_descriptions['wakeup'])
pause(2)
print(all_descriptions['whatToDo'])
pause(2)
door = chooseDoor()
pause(1)
door = checkDoor(door)
pause(1)
room = doorSelectionOutcome(door)
while (room != all_descriptions['haveDiedBeforeMessage']):
    if (room == 'Now what?'):
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionOutcome(door)
    if (room == all_descriptions['roomChecked']):
        # print(room)
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionAfterDied(door)
if (room == all_descriptions['haveDiedBeforeMessage']):
    allRoomsChecked = False
    roomsChecked = []
    print(all_descriptions['wakeup'])
    pause(2)
    print(all_descriptions['whatToDo'])
    pause(2)
    door = chooseDoor()
    door = checkDoor(door)
    room = doorSelectionAfterDied(door)
# print('Already died and moving on')
door = chooseDoor()
door = checkDoor(door)
room = doorSelectionAfterDied(door)
while (room != all_descriptions['escape'] and room != all_descriptions['checkAgain']):
    if (room == 'Now what?'):
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionAfterDied(door)
    if (room == all_descriptions['roomChecked']):
        # print(room)
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionAfterDied(door)
if (room == all_descriptions['checkAgain']):
    print(all_descriptions['checkAgain'])
    allRoomsChecked = False
    roomsChecked = [1]
room = doorSelectionSecondOutcome(door)
while (room != all_descriptions['escape'] and room != all_descriptions['checkAgain']):
    if (room == 'Now what?'):
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionSecondOutcome(door)
    if (room == all_descriptions['roomChecked']):
        # print(room)
        door = chooseDoor()
        door = checkDoor(door)
        room = doorSelectionSecondOutcome(door)
exit
