import os

# define commands that the player can use
commands = ["north", "south", "east", "west"]

# read the room descriptions from the text file
room_descriptions = {}
file_path = os.path.join(os.getcwd(), "Volkov.txt")
with open(file_path, "r") as file:
    for line in file:
        name, description, connections = line.strip().split(":")
        connections = connections.split(",")
        room_descriptions[name] = {"description": description, "connections": connections}

# define the starting room and set the current room to it
starting_room = "entryway"
current_room = starting_room

# game loop
while True:
    # print the current room's description
    print(room_descriptions[current_room]["description"])

    # get the player's input and check if it's a valid command
    command = input("What do you want to do? ").strip().lower()
    while command not in commands:
        print("Invalid command.")
        command = input("What do you want to do? ").strip().lower()

    # check if the command leads to a valid room
    if command in room_descriptions[current_room]["connections"]:
        current_room = command
    else:
        print("You can't go that way.")
