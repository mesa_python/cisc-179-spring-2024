import random


class Ghost:
    """Defines a randomly selected game objective identify this ghost

    has_evi(test): ...
    get_evi(): ...
    reveal(): ...
    """

    ghosts = ["hantu", "obake", "poltergheist", "demon", "mimic"]
    tell = ["you shiver as if a cold wind blue through you",
            "you hear wolves howling",
            "you tremble"]

    def __init__(self):
        """ docstring here """
        self.type = self.ghosts[int(random.randrange(0, 5))]
        self.evidence = ""
        self.room = room_list[random.randrange(2, 6)]
        self.get_evi()

    def has_evi(self, test):
        """ docstring here """
        ret = False
        if test == "all":
            return True
        for x in self.evidence:
            if x == test:
                ret = True
        return ret

    def __str__(self) -> str:
        return self.type + "    " + self.evidence

    def reveal(self):
        print("I am a " + self.type.upper() + " and you can tell me apart because of the " +
              self.evidence[0].upper() + " in the " + self.room.name)

    def get_evi(self):
        # FIXME: use a `dict` of `list`s data structure and the .get() method to make this a 1-liner
        if self.type == "hantu":
            self.evidence = ["freezing"]
        if self.type == "demon":
            self.evidence = ["emf"]
        if self.type == "poltergheist":
            self.evidence = ["examine"]
        if self.type == "mimic":
            self.evidence = ["freezing", "emf", "examine", "fingerprints"]
        if self.type == "obake":
            self.evidence = ["fingerprints"]

# you can find the ghost by searching through rooms of the house


class GameMap:
    """ Defines the game map (rooms and how they are connected) """

    def __init__(self, name, neighboors):
        self.name = name
        self.neighbors = neighboors

    def __eq(self, other):
        return self.name == other.name

    def inside(self):
        return self.name == current_room

    # removes the possibility of teleporting between rooms
    def is_valid_move(self, move):
        ret = False
        for x in self.neighbors:
            if x.lower() == move:
                ret = True
        return ret


# Map
###            Bedroom -- Bathroom
# |
# Door -- Hallway -- Living Room
# |
# Kitchen
room_list = [Rooms("start", ["door"]),
             Rooms("door", ["hallway"]),
             Rooms("hallway", ["bedroom", "livingroom"]),
             Rooms("bedroom", ["bathroom", "hallway"]),
             Rooms("bathroom", ["bedroom"]),
             Rooms("livingroom", ["hallway", "kitchen"]),
             Rooms("kitchen", ["livingroom"])]

# initialize ghost room
spooky = Ghost()

current_room = "start"


# next essentially is the current input for processing
next = ""

# the m_ and d_ and g_ are meant to help me separate different types of commands while parsing the command string
# the next few functions are just for displaying help related messages to teach the player the commands and game details


def help_menu():
    global next
    next = input("""Welcome to the Help Menu!
Here’s a command list:
Movement (travel around the house):
m_'room name'
ex. m_bathroom
Detection (examine your room for evidence):
d_EMF, d_Freezing, d_Fingerprints, d_Examine, d_Lost
Ghosthunting (confused about ghosts or know what the ghost is?):
g_Guess, g_Notebook
press enter to continue\n
""")
    next = ""
    room()

# teaches players about the ghosts

# FIXME: all functions should have an input and output (e.g. global variables should be input args)
# FIXME: function names are easier to understand if the are verbs that describe what the function does


def journal(input_prompt=journal_text):
    """ Display the journal_text, retrieve user input, then set/return the next_description variable """
    # FIXME: rename variable `input_prompt` or similar. Never use a built-in function name as a variable name
    # FIXME: avoid global variables by taking an input and return the
    global next
    next = input("""Welcome to your trusty Ghost hunting guide!
Ghosts are lazy they reside in one part of the house they haunt. Due to this, these ghosts
will leave behind evidence in their ghost room. The other hunters that came have narrowed 
the one haunting this down down to five types: Hantu, Poltergheist, Mimic, Obake, and Demon.
Each of these leave a unique trace:
Demon- EMF
Hantu- Freezing
Obake- Fingerprints
Poltergheist- A mess in the room i.e. thrown items
Mimic- multiple evidences
When you're ready to guess which ghost it is type in g_Guess 'ghost_type'.
BE CAREFUL YOU ONLY HAVE ONE TRY!
Press enter to continue\n
""")
    next = ""
    # FIXME: room should also accept an input argument and return the result of the function's actions
    room(next_description="")
    return ""

# handles movement commands


def change_rooms(move):
    global next
    global current_room
    for x in room_list:
        if x.name == current_room:
            if x.is_valid_move(move):
                current_room = move
                return room()

    next = input("Invalid Move please try again or type d_Lost to get a description of the room\n")
    detect_cmd()

# handles detection commands (evidences)


def explore():
    global current_room
    global spooky
    global next

    if next.find("lost") != -1:
        return room()

    if spooky.room.name.lower() != current_room:
        next = input("You look around but find nothing. What will you do next?\n")
        return detect_cmd()

    for x in spooky.evidence:
        if x == next:
            # FIXME: A linter and automatic code-formatter can help you comply with the PEP8 style guide for Python
            display = (
                ("Brrrrr y" if x == "freezing" else "Y")
                + "ou found "
                + ("an emf reading of level 5 " if x == "emf" else "")
                + ("a mess" if x == "examine" else x)
                + " in this room"
            )
            next = input(display + ". What will you do next?\n")
            return detect_cmd()

    next = input("Please try typing what you're trying to detect again. If this occurs again type help for a list of commands.\n")
    return detect_cmd()

# handles commands regarding the journal and a ghost guess


def ghost_help():
    global next
    if (next.find("notebook") != -1) or (next.find("journal") != -1):
        return journal()
    elif next.find("guess") != -1:
        next = next[5:]
        if next == spooky.type.lower():
            print("Good work hunter! Thanks for playing!")
        else:
            print("Incorrect. The ghost knows you tried to get rid of it. Everything is dark. You run for the door but\n the hallway keeps getting longer. The ghost catches you.\n Better luck next time. Thanks for playing!")
        quit()
    next = input("invalid command please try again or type help\n")
    return detect_cmd()


# whenever the user types and input this disects them into each of the primary types of commands which are categorized in the help cmd
def detect_cmd():
    # to standardize all the inputs
    global next
    next = next.replace(" ", "").lower()

    if next == "help":
        return help_menu()
    if next == "quit":
        return exit()

    if next.find("m_") != -1:
        return change_rooms(next[2:])

    if next.find("d_") != -1:
        next = next[2:]
        return explore()

    if next.find("g_") != -1:
        next = next[2:]
        return ghost_help()

    next = input("invalid command please try again or type help\n")
    return detect_cmd()


# Game Loop
def room():
    global next
    global current_room
    global spooky
    if current_room == "start":
        next = input("Hello! Welcome to PhasNOphobia. Type 'Help' for help otherwise\n type 'Ready' to begin\n").lower()
        if next == "ready":
            return change_rooms("door")
        else:
            next = "d_lost"

    if current_room == "door":
        next = input("""Welcome ghost hunter to BangleWood Drive. Your objective is to explore the haunted house and 
find out what type of ghost is haunting it. Use your detection commands in rooms 
to help you narrow down the ghost types. Remember that ghosts leave specific traces 
in their haunted rooms. If you ever forget, type 'g_Notebook' to access your
notes. Press enter to start, Happy Hunting!\n""").lower()

        print("""The door slams shut behind you. You desparately pull on the handle, but the door seems locked. 
The only way out now is to find out what kind of ghost you're dealing with so you can exorcise it. 
You have one chance. Good luck hunter.\n""")
        next = "m_hallway"

    # helps the player to locate the ghost room
    ghostly_sign = ["As you enter the room you shiver.\n",
                    "You smell a recently extinguished candle, still emitting smoke.\n",
                    "Screams echo from deeper within the house.\n",
                    "You feel eyes digging into your soul.\n",
                    "You hear a door slamming shut\n"]

    if current_room == spooky.room.name:
        print("\n" + ghostly_sign[random.randrange(0, 5)])

    # rooms in the house
    if current_room == "hallway":
        next = input("""You find yourself in a long decrepit hallway which splits off in two paths. 
On your left you can see a bedroom, and on the right you see a living room. Which way will you go?\n""")

    if current_room == "bedroom":
        next = input("""You're in a dark bedroom which splits off in two paths. There's a large bed in the middle
and a desk with a computer on top. There are two doors one leads back to the hallway and another 
leads to the bathroom. Which way will you go?\n""")

    if current_room == "bathroom":
        next = input("""The stench of an unflushed toilet fills your nostrils. You try the turn on the ventilation,
but the electric box is off. There's a five in 1 conditioner, toothpaste, body wash, shampoo, and toilet paper on the
back of the door to the bedroom. What's your next move?\n""")

    if current_room == "livingroom":
        next = input("""Entering the living room, you spot and reach for the remote. 'click' the tv doesn't respond. 
The electric box must be off, but you're too scared to venture to the basement without a flashlight.
You sit on the couch looking at another hallway that leads to the kitchen. What will you do next?\n""")

    if current_room == "kitchen":
        next = input("""As you enter the kitchen, the glint of knives on the cutting board catch your
gaze. You approach the set of knives one of which appears rusted. It eerily points 
back towards the living room where you just came from. What is your next move? \n""")

    return detect_cmd()



# starts the game loop
room()
