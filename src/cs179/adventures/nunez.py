import random


class Room:
    def __init__(self, name, description, exits, options=None, option_facts=None):
        self.name = name
        self.description = description
        self.exits = exits
        self.options = options if options else []
        self.option_facts = option_facts if option_facts else {}
        self.viewed_options = set()

    def remove_option(self, option):
        if option in self.options:
            self.options.remove(option)


class Game:
    def __init__(self):
        self.rooms = {
            "start": Room("Aquarium Entrance", "Welcome to the Mesa Aquarium. Enjoy your tour.", {"room1": "room1"}),
            "room1": Room("Deep Sea Room", "You walk into the Deep Sea Room.", {"start": "start", "room2": "room2", "room3": "room3"}, ["exhibit1", "exhibit2"], {"exhibit1": "Vampire Squid,this gentle scavenger floats through the deep sea collecting marine snow and other drifting debris with long feeding filaments.", "exhibit2": "Flapjack Octopus, when it swims, it looks like a jelly, and when it floats, it looks like an umbrella. In all its shapes, the flapjack octopus is cute — scientists even considered naming it adorabilis."}),
            "room2": Room("Open Sea Room", "You wander over to the Open Sea Room.", {"room3": "room3", "room4": "room4"}, ["exhibit1", "exhibit2"], {"exhibit1": "Scalloped Hammerhead Shark, the scalloped hammerhead commonly preys on stingrays — once, one was found with 96 venomous stingray barbs stuck in its mouth and jaws.", "exhibit2": "Dolphinfish, the dolphinfish sports iridescent body colors — metallic blues and greens on the back and sides, with white and yellow underneath. Many dolphinfish have blue, green or black spots."}),
            "room3": Room("Rocky Shore Room", "You have made your way to the Rocky Shore Room", {"room5": "room5", "room4": "room4"}, ["exhibit1", "exhibit2"], {"exhibit1": "Acorn Barnacle, the acorn barnacle begins life as a free-swimming larva. When the time comes to settle, it “glues” itself to a hard surface such as a wharf piling or ship.", "exhibit2": "Monkeyface Prickleback, this fish doesn’t move around much, seldom traveling more than 15 feet (4.6 m) from its home."}),
            "room4": Room("Kelp Forest Room", "You made it to the Kelp Forest Room.", {"room5": "room5", "room4": "room4"}, ["exhibit1", "exhibit2"], {"exhibit1": "Giant Kelpfish, individual kelpfish can change colors to match changes in the colors around them.", "exhibit2": "Swell Shark, a swell shark deals with predators by bending into a U-shape and swallowing seawater to swell to twice its normal size."}),
            "room5": Room("Sandy Shores Room", "You meander into the Sandy Shores Room", {"start": "start", "end": "end"}, ["exhibit1", "exhibit2"], {"exhibit1": "Bat Ray, a bat ray flaps its batlike wings (pectoral fins) to swim gracefully through the water — and help it uncover prey hiding in the sand.", "exhibit2": "Western Snowy Plover, the snowy plover nests in shallow nooks in the sand — sometimes even using human footprints to keep its eggs."}),
            "end": Room("End Room", "I hope you learned a lot! You've reached the end of the tour.", {}, {})
        }
        self.current_room = "start"
        self.previous_room = None
        self.has_map = False

    def play(self):
        print("Welcome to the Aquarium!")
        print("Type 'quit' to exit at any time.")
        while self.current_room != "end":
            room = self.rooms[self.current_room]
            print("\n" + room.name)
            print(room.description)
            if self.current_room == "start" and not self.has_map:
                response = input("You have approached the Help Desk. Do you want a map? (yes/no): ").lower()
                if response == "yes":
                    self.has_map = True
                    print("You have received a map!")
                else:
                    print("You have declined the map.")
                self.current_room = "room1"
                continue

            options_left = [option for option in room.options if option not in room.viewed_options]
            if options_left:
                print("Options to look at:", ", ".join(options_left))
                choice = input("Choose an option to view: ").strip().lower()
                if choice in options_left:
                    print(room.option_facts[choice])
                    room.viewed_options.add(choice)
                else:
                    print("Invalid option. Try again.")
                    continue
            else:
                print("You've viewed all the options. Choose a room to move.")
                print("Available rooms:", ", ".join(room.exits.keys()))
                direction = input("Choose a room to move: ").strip().lower()
                if direction == "quit":
                    print("Thanks for playing!")
                    return
                if direction in self.rooms[self.current_room].exits:
                    self.current_room = self.rooms[self.current_room].exits[direction]
                else:
                    print("Invalid room. Try again.")

        print("\n" + self.rooms["end"].description)


if __name__ == "__main__":
    game = Game()
    game.play()
import random


class Room:
    def __init__(self, name, description, exits, options=None, option_facts=None):
        self.name = name
        self.description = description
        self.exits = exits
        self.options = options if options else []
        self.option_facts = option_facts if option_facts else {}
        self.viewed_options = set()

    def remove_option(self, option):
        if option in self.options:
            self.options.remove(option)


class Game:
    def __init__(self):
        self.rooms = {
            "start": Room("Aquarium Entrance", "Welcome to the Mesa Aquarium. Enjoy your tour.", {"room1": "room1"}),
            "room1": Room("Deep Sea Room", "You walk into the Deep Sea Room.", {"start": "start", "room2": "room2", "room3": "room3"}, ["exhibit1", "exhibit2"], {"exhibit1": "Vampire Squid,this gentle scavenger floats through the deep sea collecting marine snow and other drifting debris with long feeding filaments.", "exhibit2": "Flapjack Octopus, when it swims, it looks like a jelly, and when it floats, it looks like an umbrella. In all its shapes, the flapjack octopus is cute — scientists even considered naming it adorabilis."}),
            "room2": Room("Open Sea Room", "You wander over to the Open Sea Room.", {"room3": "room3", "room4": "room4"}, ["exhibit1", "exhibit2"], {"exhibit1": "Scalloped Hammerhead Shark, the scalloped hammerhead commonly preys on stingrays — once, one was found with 96 venomous stingray barbs stuck in its mouth and jaws.", "exhibit2": "Dolphinfish, the dolphinfish sports iridescent body colors — metallic blues and greens on the back and sides, with white and yellow underneath. Many dolphinfish have blue, green or black spots."}),
            "room3": Room("Rocky Shore Room", "You have made your way to the Rocky Shore Room", {"room5": "room5", "room4": "room4"}, ["exhibit1", "exhibit2"], {"exhibit1": "Acorn Barnacle, the acorn barnacle begins life as a free-swimming larva. When the time comes to settle, it “glues” itself to a hard surface such as a wharf piling or ship.", "exhibit2": "Monkeyface Prickleback, this fish doesn’t move around much, seldom traveling more than 15 feet (4.6 m) from its home."}),
            "room4": Room("Kelp Forest Room", "You made it to the Kelp Forest Room.", {"room5": "room5", "room4": "room4"}, ["exhibit1", "exhibit2"], {"exhibit1": "Giant Kelpfish, individual kelpfish can change colors to match changes in the colors around them.", "exhibit2": "Swell Shark, a swell shark deals with predators by bending into a U-shape and swallowing seawater to swell to twice its normal size."}),
            "room5": Room("Sandy Shores Room", "You meander into the Sandy Shores Room", {"start": "start", "end": "end"}, ["exhibit1", "exhibit2"], {"exhibit1": "Bat Ray, a bat ray flaps its batlike wings (pectoral fins) to swim gracefully through the water — and help it uncover prey hiding in the sand.", "exhibit2": "Western Snowy Plover, the snowy plover nests in shallow nooks in the sand — sometimes even using human footprints to keep its eggs."}),
            "end": Room("End Room", "I hope you learned a lot! You've reached the end of the tour.", {}, {})
        }
        self.current_room = "start"
        self.previous_room = None
        self.has_map = False

    def play(self):
        print("Welcome to the Aquarium!")
        print("Type 'quit' to exit at any time.")
        while self.current_room != "end":
            room = self.rooms[self.current_room]
            print("\n" + room.name)
            print(room.description)
            if self.current_room == "start" and not self.has_map:
                response = input("You have approached the Help Desk. Do you want a map? (yes/no): ").lower()
                if response == "yes":
                    self.has_map = True
                    print("You have received a map!")
                else:
                    print("You have declined the map.")
                self.current_room = "room1"
                continue

            options_left = [option for option in room.options if option not in room.viewed_options]
            if options_left:
                print("Options to look at:", ", ".join(options_left))
                choice = input("Choose an option to view: ").strip().lower()
                if choice in options_left:
                    print(room.option_facts[choice])
                    room.viewed_options.add(choice)
                else:
                    print("Invalid option. Try again.")
                    continue
            else:
                print("You've viewed all the options. Choose a room to move.")
                print("Available rooms:", ", ".join(room.exits.keys()))
                direction = input("Choose a room to move: ").strip().lower()
                if direction == "quit":
                    print("Thanks for playing!")
                    return
                if direction in self.rooms[self.current_room].exits:
                    self.current_room = self.rooms[self.current_room].exits[direction]
                else:
                    print("Invalid room. Try again.")

        print("\n" + self.rooms["end"].description)


if __name__ == "__main__":
    game = Game()
    game.play()
