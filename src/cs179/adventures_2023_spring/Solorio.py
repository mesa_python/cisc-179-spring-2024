def start_game():
    print("Welcome to the text adventure game!")
    print("You wake up in a dark room. You can't see anything.")
    print("What do you want to do?")
    print("1. Look around.")
    print("2. Try to find a light switch.")
    print("3. Scream for help.")

    choice = input("Enter your choice (1, 2, or 3): ")

    if choice == "1":
        print("You reach around in the darkness and eventually find a door.")
        room_2()
    elif choice == "2":
        print("You find a light switch and turn on the lights.")
        room_1()
    elif choice == "3":
        print("Nobody hears you. You are stuck here forever.")
        end_game()

def room_1():
    print("You are in a room with a table and a chair.")
    print("There is a piece of paper on the table.")
    has_read_paper = False
    while True:
        print("What do you want to do?")
        print("1. Read the paper.")
        print("2. Look under the table.")
        print("3. Leave the room.")
        choice = input("Enter your choice (1, 2, or 3): ")
        if choice == "1":
            if not has_read_paper:
                print("The paper reads: 'The code is 2012'.")
                has_read_paper = True
            else:
                print("You have already read the paper.")
        elif choice == "2":
            print("You find a key under the table.")
            room_2()
        elif choice == "3":
            start_game()


def room_2():
    print("You are in a hallway.")
    print("There are two doors, one on the left and one on the right.")
    print("What do you want to do?")
    print("1. Try the door on the left.")
    print("2. Try the door on the right.")
    print("3. Go back to the previous room.")

    choice = input("Enter your choice (1, 2, or 3): ")

    if choice == "1":
        print("You try the door on the left, but it's locked.")
        room_2()
    elif choice == "2":
        print("You try the door on the right, and it opens into a treasure room!")
        end_game()
    elif choice == "3":
        room_1()

def end_game():
    print("Congratulations, you have won the game!")

# Start the game
start_game()

