// Mesa College Custom Global Navigation Button For Student Support
// With custom icon
// Edited: 05-02-2022


(function () {
  'use strict';
  // configure links
  const links = [
    {
      title: 'Student Support',
      icon_svg: 'icon-discussion',
      href: 'https://www.sdmesa.edu/student-support/',
      target: '_blank'
    }, // ready for another
  ];
  // leave this alone
  const globalNavCustomLinks=e=>{let t=(e,t)=>{e.setAttribute("id",`global_nav_${t}_svg`),e.setAttribute("class","ic-icon-svg menu-item__icon ic-icon-svg--apps svg-icon-help ic-icon-svg-custom-tray"),e.getAttribute("height")>"26px"&&e.removeAttribute("height"),e.getAttribute("width")>"26px"&&e.removeAttribute("width")};e.forEach(e=>{const i=e.title.replace(/\W/g,"_").toLowerCase();var n=$("<li>",{id:`global_nav_${i}_link`,class:"ic-app-header__menu-list-item",html:`<a id="global_nav_${i}_link" role="button" href="${e.href}" target="${e.target}" class="ic-app-header__menu-list-link">\n            <div class="menu-item-icon-container" role="presentation"><span class="svg-${i}-holder"></span></div>\n            <div class="menu-item__text">${e.title}</div></a>`});if(1==/^icon-[a-z]/.test(e.icon_svg))n.find(`.svg-${i}-holder`).append($("<div>",{id:`global_nav_${i}_svg`,class:"menu-item-icon-container",html:`<i class="icon-line ${e.icon_svg} gnct_inst_menu_icon"></i></div>`,role:"presentation"}));else if(/^http/.test(e.icon_svg))n.find(`.svg-${i}-holder`).load(e.icon_svg,(function(){let e=$(this).find("svg")[0];t(e,i)}));else if(/^<svg/.test(e.icon_svg)){n.find(`.svg-${i}-holder`).append($(e.icon_svg));let s=n.find(`.svg-${i}-holder`).find("svg")[0];t(s,i)}$("#menu").append(n)})};

  // handle css, you can remove or comment the next line if you're also using Global Nav Custom Tray
  !function(){if(0==document.querySelectorAll('[data-global-nav-custom-css="set"]').length){let e={"i.gnct_inst_menu_icon:before":"font-size: 26px; width: 26px; line-height: 26px;","i.gnct_inst_menu_icon":"width: 26px; height: 26px;"};if(void 0!==e&&Object.keys(e).length>0){let t=document.createElement("style");t.setAttribute("data-global-nav-custom-css","set"),document.head.appendChild(t);let i=t.sheet;Object.keys(e).forEach((function(t){i.insertRule(`${t} { ${e[t]} }`,i.cssRules.length)}))}}}();
  
// add links to menu
  globalNavCustomLinks(links);
})();

